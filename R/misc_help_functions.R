################################################################################
#                                                                              #
# This script contains scripts that the main function(s) uses. It is not to    #
# be exported                                                                  #
#                                                                              #
################################################################################

############ Function to determine if the enviroment is INCA or not ############
is.inca <- function(){
  unname((Sys.info()["nodename"] %in% c("EXT-R27-PROD","EXT-R37-TEST")))
}
